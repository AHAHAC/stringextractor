import org.apache.commons.io.FileUtils
import java.io.File
import java.nio.charset.Charset

class LangStringProcessor(var extractor: Extractor) {

    private val SELECT_STRINGS_REGEX = Regex("\".*?\"")
    private var cachedStrings = hashMapOf<String, String>()


    fun start(path: File) {
        processDir(path)
        createLangFile(path)
    }

    private fun createLangFile(path: File) {
        val fileOut = extractor.createLocalizedFile(cachedStrings)
        val langFile = File("${path.absolutePath}/${extractor.getLocalizationFileName()}")
        FileUtils.writeStringToFile(langFile, fileOut, Charset.defaultCharset())
    }

    private fun processDir(dir: File?) {
        println("dir = ${dir?.absolutePath}")
        dir?.listFiles()?.forEach { file ->
            if (file.isDirectory) {
                processDir(file)
            } else {
                if (extractor.isNeedProcessFile(file)) {
                    processFile(file)
                }
            }
        }
    }

    private fun processFile(file: File) {
        println("file = ${file.absolutePath}")
        val charset = Charset.defaultCharset()
        val fileContent = FileUtils.readFileToString(file, charset)
        val updatedFileContent = SELECT_STRINGS_REGEX.replace(fileContent, { matchResult ->
            val value = matchResult.value
            if (!extractor.isNeedProcessString(value)) {
                return@replace value
            }
            return@replace processString(value)
        })
        FileUtils.writeStringToFile(file, updatedFileContent, charset)
    }

    private fun processString(value: String): String {
        val value = value.substring(1, value.lastIndex)
        val localizedFileKey = extractor.localizedFileKey(value)
        val localizedFileValue = extractor.localizedFileValue(value)
        val updatedCodeFileValue = extractor.updatedCodeFileValue(value)
        cachedStrings[localizedFileKey] = localizedFileValue
        return updatedCodeFileValue
    }
}