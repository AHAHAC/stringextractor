import java.io.File

class SwiftExtractor : Extractor {

    private val SELECT_INSTRING_EXPRESSION_REGEX = Regex("\\\\\\(([^\\\\)]+.)[\\\\)]")

    override fun isNeedProcessFile(file: File): Boolean {
        return file.extension == "swift"
    }

    override fun isNeedProcessString(value: String): Boolean {
        return value.hasCyrillic()
    }

    override fun localizedFileKey(value: String): String {
        return value.transliterate()
    }

    override fun localizedFileValue(value: String): String {
        return SELECT_INSTRING_EXPRESSION_REGEX.replace(value.replace("%", "%%"), { _ ->
            "%@"
        })
    }

    override fun updatedCodeFileValue(value: String): String {
        val transliterated = value.transliterate()
        val currentStringArguments = SELECT_INSTRING_EXPRESSION_REGEX.findAll(value).map { it.groups[1]?.value }.toList()
        return when (currentStringArguments.size) {
            0 -> "\"$transliterated\".localized()"
            else -> {
                val args = currentStringArguments.joinToString(", ")
                "String(format: \"$transliterated\".localized(), $args)"
            }
        }
    }

    override fun createLocalizedFile(localization: Map<String, String>): String {
        return localization.map { (key, value) -> "\"$key\" = \"$value\";" }.joinToString { "\n" }
    }

    override fun getLocalizationFileName(): String {
        return "Localizable.strings"
    }
}