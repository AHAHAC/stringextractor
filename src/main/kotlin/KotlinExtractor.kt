import java.io.File

class KotlinExtractor : Extractor {

    private val SELECT_INSTRING_EXPRESSION_REGEX = Regex("\\\$([\\w]+)|\\{([^\\}]+.)\\}")

    override fun isNeedProcessFile(file: File): Boolean {
        return file.extension == "kt"
    }

    override fun isNeedProcessString(value: String): Boolean {
        return value.hasCyrillic()
    }

    override fun localizedFileKey(value: String): String {
        return value.transliterate()
    }

    override fun localizedFileValue(value: String): String {
        return Regex("\\\$([\\w]+|\\{[^\\}]+.\\})").replace(value.replace("%", "%%"), {
            "%s"
        })
    }

    override fun updatedCodeFileValue(value: String): String {
        val transliterated = value.transliterate()
        val currentStringArguments = SELECT_INSTRING_EXPRESSION_REGEX.findAll(value).map {
            (it.groups[2] ?: it.groups[1])?.value
        }.toList()
        return when (currentStringArguments.size) {
            0 -> "getString(R.string.$transliterated)"
            else -> {
                val args = currentStringArguments.joinToString(separator = ", ") { "$it?.toString()" }
                "String.format(getString(R.string.$transliterated), $args)"
            }
        }
    }

    override fun createLocalizedFile(localization: Map<String, String>): String {
        return "<resources>\n".plus(localization.map {
            "    <string name=\"${it.key}\">${it.value}</string>"
        }.sortedBy { it.length }.joinToString("\n")).plus("\n</resources>")
    }

    override fun getLocalizationFileName(): String {
        return "strings.xml"
    }
}