import java.io.File

interface Extractor {


    /**
     * @return нужно ли обрабатывать файл
     */
    fun isNeedProcessFile(file: File): Boolean

    /**
     * @param value - найденая строка в обрабатываемом файле '"example"'
     * @return нужно ли обрабатывать строку
     */
    fun isNeedProcessString(value: String): Boolean

    /**
     * @param value - найденая строка в обрабатываемом файле '"example"'
     * @return ключ, который будет в файле перевода
     */
    fun localizedFileKey(value: String): String

    /**
     * @param value - найденая строка в обрабатываемом файле '"example"'
     * @return значение со знаками форматирования, которое будет в файле перевода
     */
    fun localizedFileValue(value: String): String

    /**
     * @param value - найденая строка в обрабатываемом файле '"example"'
     * @return значение, которое заменит исходную строку в обрабатываемом файле
     */
    fun updatedCodeFileValue(value: String): String

    /**
     * @param localization - ключ и значение для файла перевода
     * @return отформатированный файл перевода
     */
    fun createLocalizedFile(localization: Map<String, String>): String

    /**
     * @return имя файла перевода
     */
    fun getLocalizationFileName(): String
}