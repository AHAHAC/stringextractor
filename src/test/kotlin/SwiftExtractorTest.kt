import junit.framework.Assert
import junit.framework.TestCase
import java.io.File

class SwiftExtractorTest : TestCase() {

    private val extractor = SwiftExtractor()

    fun testIsNeedProcessFile() {
        assertFalse(extractor.isNeedProcessFile(File("file")))
        assertFalse(extractor.isNeedProcessFile(File("file.json")))
        assertFalse(extractor.isNeedProcessFile(File("file.kt")))
        assertFalse(extractor.isNeedProcessFile(File("file.java")))
        assertFalse(extractor.isNeedProcessFile(File("file.cpp")))
        assertTrue(extractor.isNeedProcessFile(File("file.swift")))
    }

    fun testIsNeedProcessString() {
        assertFalse(extractor.isNeedProcessString("test"))
        assertFalse(extractor.isNeedProcessString("1234"))
        assertFalse(extractor.isNeedProcessString("!@#$%^"))
        assertFalse(extractor.isNeedProcessString("213 fwfmkmk kmm \n 123"))
        assertTrue(extractor.isNeedProcessString("тест"))
        assertTrue(extractor.isNeedProcessString("тест 123"))
    }

    fun testLocalizedFileKey() {
        assertEquals("test", extractor.localizedFileKey("тест"))
        assertEquals("test_1234567890", extractor.localizedFileKey("тест 1234567890"))
        assertEquals("test_test", extractor.localizedFileKey("тест test"))
        assertEquals("_1test", extractor.localizedFileKey("1тест"))
    }

    fun testLocalizedFileValue() {
        assertEquals("тест", extractor.localizedFileValue("тест"))
        assertEquals("тест %@", extractor.localizedFileValue("тест \\(123)"))
        assertEquals("тест %@ 123 %@ test!", extractor.localizedFileValue("тест \\(123) 123 \\(call()) test!"))
    }

    fun testUpdatedCodeFileValue() {
        Assert.assertEquals("\"test\".localized()", extractor.updatedCodeFileValue("тест"))
        Assert.assertEquals("\"check\".localized()", extractor.updatedCodeFileValue("check"))
        Assert.assertEquals("String(format: \"test_123\".localized(), 123)", extractor.updatedCodeFileValue("тест \\\\(123)"))
        Assert.assertEquals("String(format: \"test_123_123_call_test\".localized(), 123, call())", extractor.updatedCodeFileValue("тест \\(123) 123 \\(call()) test!"))
    }
}