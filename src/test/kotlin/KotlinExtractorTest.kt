import junit.framework.Assert
import junit.framework.TestCase
import java.io.File

class KotlinExtractorTest : TestCase() {

    private val extractor = KotlinExtractor()

    fun testIsNeedProcessFile() {
        assertFalse(extractor.isNeedProcessFile(File("file")))
        assertFalse(extractor.isNeedProcessFile(File("file.json")))
        assertFalse(extractor.isNeedProcessFile(File("file.java")))
        assertFalse(extractor.isNeedProcessFile(File("file.cpp")))
        assertFalse(extractor.isNeedProcessFile(File("file.swift")))
        assertTrue(extractor.isNeedProcessFile(File("file.kt")))
    }

    fun testIsNeedProcessString() {
        assertFalse(extractor.isNeedProcessString("test"))
        assertFalse(extractor.isNeedProcessString("1234"))
        assertFalse(extractor.isNeedProcessString("!@#$%^"))
        assertFalse(extractor.isNeedProcessString("213 fwfmkmk kmm \n 123"))
        assertTrue(extractor.isNeedProcessString("тест"))
        assertTrue(extractor.isNeedProcessString("тест 123"))
    }

    fun testLocalizedFileKey() {
        assertEquals("test", extractor.localizedFileKey("тест"))
        assertEquals("test_1234567890", extractor.localizedFileKey("тест 1234567890"))
        assertEquals("test_test", extractor.localizedFileKey("тест test"))
        assertEquals("_1test", extractor.localizedFileKey("1тест"))
    }

    fun testLocalizedFileValue() {
        assertEquals("тест", extractor.localizedFileValue("тест"))
        assertEquals("тест %s", extractor.localizedFileValue("тест \$123"))
        assertEquals("тест %s", extractor.localizedFileValue("тест \${123}"))
        assertEquals("тест %s 123 %s test!", extractor.localizedFileValue("тест \$123 123 \${call()} test!"))
    }

    fun testUpdatedCodeFileValue() {
        assertEquals("getString(R.string.test)", extractor.updatedCodeFileValue("тест"))
        assertEquals("getString(R.string.check)", extractor.updatedCodeFileValue("check"))
        assertEquals("String.format(getString(R.string.test_123), 123?.toString())", extractor.updatedCodeFileValue("тест \$123"))
        assertEquals("String.format(getString(R.string.test_123), 123?.toString())", extractor.updatedCodeFileValue("тест \${123}"))
        assertEquals("String.format(getString(R.string.test_123_123_call_test), 123?.toString(), call()?.toString())", extractor.updatedCodeFileValue("тест \$123 123 \${call()} test!"))
        assertEquals("String.format(getString(R.string.test_123_123_call_test), 123?.toString(), call()?.toString())", extractor.updatedCodeFileValue("тест \${123} 123 \${call()} test!"))
        assertEquals("getString(R.string._5min)", extractor.updatedCodeFileValue("5мин"))
    }

    fun testCreateLocalizedFile() {
        val trueFile =
                """<resources>
    <string name="test">тест</string>
</resources>"""

        Assert.assertEquals(trueFile, extractor.createLocalizedFile(mapOf("test" to "тест")))
    }

}