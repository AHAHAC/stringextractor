import javafx.application.Application
import javafx.stage.DirectoryChooser
import javafx.stage.Stage
import kotlin.system.exitProcess

class WindowApp : Application() {


    override fun start(primaryStage: Stage?) {
        val fileChooser = DirectoryChooser()
        fileChooser.title = "Select directory"
        val path = fileChooser.showDialog(primaryStage)
        println("path = $path")
        if (path == null) {
            exitProcess(0)
        }
        val processor = LangStringProcessor(KotlinExtractor())
        processor.start(path)
        exitProcess(0)
    }

    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            launch(WindowApp::class.java)
        }
    }
}